#include <iostream>

#include "managerview.h"
#include "managerservice_impl.h"
#include "managerview_console_impl.h"
#include "tools.h"
//#include "manager.h"

#include <string.h>
//#include "emis.h"

using namespace std;
using namespace MH;

//extern vector<Manager> v_manager;

ManagerView::~ManagerView(void){};//

ManagerViewConsoleImpl::ManagerViewConsoleImpl(void)
{
	service = new ManagerServiceImpl;
}

ManagerViewConsoleImpl::~ManagerViewConsoleImpl(void)
{
	delete service;
}

void ManagerViewConsoleImpl::menu(void)
{
	int cmd;
	system("clear");
	while(1)
	{
		//system("clear");//
		cout << "==== 业务系统 ====" << endl << endl;
		cout << "1.添加管理员" << endl;
		cout << "2.删除管理员" << endl;
		cout << "3.管理员列表" << endl << endl;
		cout << "0.退出系统" << endl;
		cout << ">";
		cin >> cmd;
		switch(cmd)
		{
			case 1 : add();break;
			case 2 : del();break;
			case 3 : list();break;
			case 0 : return;
			default: continue;
		}
	}
}

void ManagerViewConsoleImpl::add(void)
{
	Tools t;
	int id = t.generator_MarId();
	char name[20] = {};
	char password[20] = {};
	system("clear");
	cout << "====添加管理员====" << endl;
	cout << "姓名 >";
	cin >> name;
	cout << "密码 >";
	cin >> password;
	Manager m(id,name,password);
	service->addManager(m);
}

void ManagerViewConsoleImpl::del(void)
{
	int id;
	system("clear");
	cout << "====删除管理员====" << endl;
	cout << "想要删除管理员的id >";
	cin >> id;
	service->deleteManager(id);
}

void ManagerViewConsoleImpl::list(void)
{
	system("clear");
	cout << "====管理员列表====" << endl;
	service->listManager();
}

bool ManagerViewConsoleImpl::root_check(void)
{
	system("clear");
	cout << "====超级管理员登录====" << endl;
	return service->root_check();
}

bool ManagerViewConsoleImpl::mana_check(void)
{
	system("clear");
	cout << "====管理员登录====" << endl;
	return service->mana_check();
}
