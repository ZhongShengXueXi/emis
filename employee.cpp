#include <iostream>
#include "employee.h"

#include <string.h>

using namespace std;
using namespace MH;

Employee::Employee(int _e_id,const char* _e_name,bool _e_sex,int _e_age)
{
	e_id = _e_id;
	strcpy(e_name,_e_name);
	e_sex = _e_sex;
	e_age = _e_age;
}

Employee::Employee(const Employee& em)
{
	e_id = em.e_id;
	strcpy(e_name,em.e_name);
	e_sex = em.e_sex;
	e_age = em.e_age;
}

void Employee::operator = (const Employee& em)
{
	e_id = em.e_id;
	strcpy(e_name,em.e_name);
	e_sex = em.e_sex;
	e_age = em.e_age;
}

int Employee::getId(void)
{
	return e_id;
}

char* Employee::getName(void)
{
	return e_name;
}

bool Employee::getSex(void)
{
	return e_sex;
}

int Employee::getAge(void)
{
	return e_age;
}

bool Employee::setName(const char* name)
{
	strcpy(e_name,name);
	return true;
}

bool Employee::setSex(bool sex)
{
	e_sex = sex;
	return true;
}

bool Employee::setAge(int age)
{
	e_age = age;
	return true;
}