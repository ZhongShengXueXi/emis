#ifndef SERVICE_H
#define SERVICE_H

#include "department.h"
#include "employee.h"

namespace MH{

class Service
{
public:
	virtual ~Service(void) = 0;
	virtual bool addDept(Department* de) = 0;
	virtual bool deleteDept(int id) = 0;
	virtual void listDept(void) = 0;
	virtual bool addEmp(Employee* em) = 0;
	virtual bool deleteEmp(int id) = 0;
	virtual bool modifyEmp(int id) = 0;
	virtual void listEmp(void) = 0;
	virtual void listAllEmp(void) = 0;
};

}

#endif //SERVICE_H