#ifndef SERVICE_IMPL_H
#define SERVICE_IMPL_H

#include <vector>
#include "service.h"
#include "department.h"
#include "employee.h"
#include "servicedao.h"

using namespace std;

namespace MH{

class ServiceImpl : public Service
{
	ServiceDao* dao;
	vector<Department*> v_departmentp;
public:
	ServiceImpl(void);
	~ServiceImpl(void);
	bool addDept(Department* de);
	bool deleteDept(int id);
	void listDept(void);
	bool addEmp(Employee* em);
	bool deleteEmp(int id);
	bool modifyEmp(int id);
	void listEmp(void);
	void listAllEmp(void);
};

}
	
#endif //SERVICE_IMPL_H