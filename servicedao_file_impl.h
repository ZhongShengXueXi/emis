#ifndef SERVICEDAO_FILE_IMPL_H
#define SERVICEDAO_FILE_IMPL_H

#include <vector>
#include "department.h"
#include "servicedao.h"

using namespace std;

namespace MH{

class ServiceDaoFileImpl : public ServiceDao
{
public:
	void load(vector<Department*>& v_de);
	void save(vector<Department*>& v_de);
};

}

#endif //SERVICEDAO_FILE_IMPL_H