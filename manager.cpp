#include <string.h>
#include "manager.h"

using namespace MH;

Manager::Manager(int _id,const char* _name,const char* _password)
{
	id = _id;
	strcpy(name,_name);
	strcpy(password,_password);
}

Manager::Manager(const Manager& m)
{
	id = m.id;
	strcpy(name,m.name);
	strcpy(password,m.password);
}

void Manager::operator = (const Manager& m)
{
	id = m.id;
	strcpy(name,m.name);
	strcpy(password,m.password);
}

int Manager::getId(void)
{
	return id;
}

char* Manager::getName(void)
{
	return name;
}

char* Manager::getPassword(void)
{
	return password;
}
