#include <iostream>
#include <vector>
#include "serviceview.h"
#include "serviceview_console_impl.h"
#include "service_impl.h"
#include "tools.h"

#include "employee.h"
#include "department.h"

using namespace std;
using namespace MH;

ServiceView::~ServiceView(void){}

ServiceViewConsoleImpl::ServiceViewConsoleImpl(void)
{
	service = new ServiceImpl;
}

ServiceViewConsoleImpl::~ServiceViewConsoleImpl(void)
{
	delete service;
}

void ServiceViewConsoleImpl::menu(void)
{
	system("clear");
	int cmd;
	while(1)
	{
		cout << "==== 业务系统界面 ====" << endl << endl;
		cout << "	1.添加部门" << endl;
		cout << "	2.删除部门" << endl;
		cout << "	3.部门列表" << endl;
		cout << "	4.添加员工" << endl;
		cout << "	5.删除员工" << endl;
		cout << "	6.修改员工" << endl;
		cout << "	7.员工列表" << endl;
		cout << "	8.全部员工列表" << endl << endl;
		cout << "	0.quit" << endl;
		cout << ">";
		cin >> cmd;
		switch(cmd)
		{
			case 1 : addDept();break;
			case 2 : deleteDept();break;
			case 3 : listDept();break;
			case 4 : addEmp();break;
			case 5 : deleteEmp();break;
			case 6 : modifyEmp();break;
			case 7 : listEmp();break;
			case 8 : listAllEmp();break;
			case 0 : return;
			default: continue;
		}
	}
}

void ServiceViewConsoleImpl::addDept(void)
{
	Tools t;
	int id = t.generator_DeptId();
	char name[20] = {};
	system("clear");//
	cout << "==== 添加部门 ====" << endl;
	cout << "部门名称 >";
	cin >> name;

	Department* dep = new Department(id,name);
	service->addDept(dep);
}

void ServiceViewConsoleImpl::deleteDept(void)
{
	int id;
	system("clear");//
	cout << "==== 删除部门 ====" << endl;
	cout << "想要删除部门的id >";
	cin >> id;

	service->deleteDept(id); 
}

void ServiceViewConsoleImpl::listDept(void)
{
	system("clear");//
	cout << "==== 部门列表 ====" << endl;
	service->listDept();
}

void ServiceViewConsoleImpl::addEmp(void)
{
	Tools t;
	int id = t.generator_EmpId();
	char name[20] = {};
	bool sex;
	int age;
	system("clear");//
	cout << "==== 添加员工 ====" << endl;
	
	cout << "员工名字 >";
	cin >> name;
	cout << "员工性别(1:男,0:女) >";
	cin >> sex;
	cout << "员工年龄 >";
	cin >> age;

	Employee* emp = new Employee(id,name,sex,age);
	service->addEmp(emp);
}

void ServiceViewConsoleImpl::deleteEmp(void)
{
	int id;
	system("clear");
	cout << "==== 删除员工 ====" << endl;
	cout << "想要删除员工的id >";
	cin >> id;

	service->deleteEmp(id);
}

void ServiceViewConsoleImpl::modifyEmp(void)
{
	int id;
	system("clear");
	cout << "==== 修改员工 ====" << endl;
	cout << "想要修改员工的id >";
	cin >> id;

	service->modifyEmp(id);
}

void ServiceViewConsoleImpl::listEmp(void)
{
	system("clear");
	cout << "==== 员工列表 ====" << endl;
	service->listEmp();
}

void ServiceViewConsoleImpl::listAllEmp(void)
{
	system("clear");
	cout << "==== 全体员工列表 ====" << endl;
	service->listAllEmp();
}
