#ifndef MANAGERVIEW_H
#define MANAGERVIEW_H

namespace MH{
	
class ManagerView
{
public:
	virtual ~ManagerView(void) = 0;
	virtual void menu(void) = 0;
	virtual void add(void) = 0;
	virtual void del(void) = 0;
	virtual void list(void) = 0;
	virtual bool root_check(void) = 0;
	virtual bool mana_check(void) = 0;
};		
	
}

#endif //MANAGERVIEW_H
