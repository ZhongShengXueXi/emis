#ifndef SERVICEDAO_H
#define SERVICEDAO_H

#include <vector>
#include "department.h"

using namespace std;

namespace MH{

class ServiceDao
{
public:
	virtual void load(vector<Department*>& v_de) = 0;
	virtual void save(vector<Department*>& v_de) = 0;
};

}

#endif //SERVICEDAO_H