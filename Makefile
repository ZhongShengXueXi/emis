# 获取编译的源码
SRC	:=$(wildcard *.cpp)
OBJ :=$(patsubst %.cpp,%.o,$(SRC))

# 编译参数
CC	:=g++
STD	:=-std=c++0x
FLAG:=-Wall -Werror
BIN=emis

all:$(OBJ)
	$(CC) $(OBJ) -o $(BIN)

%.o:%.cpp
	$(CC) $(STD) $(FLAG) -c $<

clean:
	rm $(OBJ) $(BIN)
