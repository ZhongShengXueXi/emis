#ifndef MANAGERVIEW_CONSOLE_IMPL_H
#define MANAGERVIEW_CONSOLE_IMPL_H

#include "managerservice.h"
#include "managerview.h"

using namespace MH;

namespace MH{

class ManagerViewConsoleImpl : public ManagerView
{
	ManagerService* service;
public:
	ManagerViewConsoleImpl(void);
	~ManagerViewConsoleImpl(void);
	void menu(void);
	void add(void);
	void del(void);
	void list(void);
	bool root_check(void);
	bool mana_check(void);	
};
	
}

#endif //MANAGERVIEW_CONSOLE_IMPL_H
