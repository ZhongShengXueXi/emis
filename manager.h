#ifndef MANAGER_H
#define MANAGER_H

namespace MH {
	
class Manager
{
	int id;
	char name[20];
	char password[20];
public:
	Manager(int _id=0,const char* _name="empty",const char* _password="empty");
	Manager(const Manager& m);
	void operator = (const Manager& m);
	int getId(void);
	char* getName(void);
	char* getPassword(void);
};

}

#endif //MANAGER_H
