#include <iostream>
#include "emis.h"
#include "tools.h"
#include "managerdao_file_impl.h"

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

using namespace std;
using namespace MH;

extern vector<Manager> v_manager;

void ManagerDaoFileImpl::load(void)
{
	int fd = open(MA_PATH,O_RDONLY);
	if(0 > fd)
	{
		perror("open_m_load");
		return;
	}
	Manager tmp;
	while(1)
	{
		int ret = read(fd,&tmp,sizeof(Manager));
		if(0 == ret) break;
		//cout << tmp.getName() << endl;
		v_manager.push_back(tmp);
	}
	close(fd);
}

void ManagerDaoFileImpl::save(void)
{
	int fd = open(MA_PATH,O_WRONLY|O_TRUNC);
	if(0 > fd)
	{
		perror("open_m_save");
		return;
	}
	Manager tmp;
	while(1)
	{
		//cout << v_manager.size() << endl;
		if(v_manager.empty()) break;
		tmp = v_manager.back();
		v_manager.pop_back();
		write(fd,&tmp,sizeof(Manager));
	}
	close(fd);
}