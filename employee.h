#ifndef EMPLOYEE_H
#define EMPLOYEE_H

using namespace std;

namespace MH{

class Employee
{
	int e_id;
	char e_name[20];
	bool e_sex;
	int e_age;
public:
	Employee(int _e_id=0,const char* _e_name="",
		bool _e_sex=true,int _e_age=18);
	Employee(const Employee& em);
	void operator = (const Employee& em);

	int getId(void);
	char* getName(void);
	bool getSex(void);
	int getAge(void);

	bool setName(const char* name);
	bool setSex(bool sex);
	bool setAge(int age);
};

}

#endif //EMPLOYEE_H