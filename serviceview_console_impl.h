#ifndef SERVICEVIEW_CONSOLE_IMPL_H
#define SERVICEVIEW_CONSOLE_IMPL_H

#include "service.h"
#include "serviceview.h"

namespace MH{

class ServiceViewConsoleImpl : public ServiceView
{
	Service* service;
public:
	ServiceViewConsoleImpl(void);
	~ServiceViewConsoleImpl(void);
	void menu(void);
	void addDept(void);
	void deleteDept(void);
	void listDept(void);
	void addEmp(void);
	void deleteEmp(void);
	void modifyEmp(void);
	void listEmp(void);
	void listAllEmp(void);
};

}

#endif //SERVICEVIEW_CONSOLE_IMPL_H
