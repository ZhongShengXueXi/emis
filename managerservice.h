#ifndef MANAGERSERVICE_H
#define MANAGERSERVICE_H

#include "manager.h"

namespace MH{

class ManagerService
{
public:
	virtual ~ManagerService(void) = 0;
	virtual void addManager(Manager& m) = 0;
	virtual void deleteManager(int id) = 0;
	virtual void listManager(void) = 0;
	virtual bool root_check(void) = 0;
	virtual bool mana_check(void) = 0;
};

}

#endif //MANAGERSERVICE_H