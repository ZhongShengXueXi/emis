#ifndef DEPARTMENT_H
#define DEPARTMENT_H

#include <vector>
#include "employee.h"

using namespace std;

namespace MH{

class Department
{
	int d_id;
	char d_name[20];
	vector<Employee*> v_employeep;
public:
	Department(int _d_id=0,const char* _d_name="");
	~Department(void);
	Department(const Department& de);
	void operator = (const Department& de);
	
	int getId(void);
	char* getName(void);
	vector<Employee*>& getV_employeep(void);

	bool addEmp(Employee* em);
	bool deleteEmp(int id);
	void listEmp(void);
	bool modifyEmp(int id);
};

}

#endif //DEPARTMENT_H