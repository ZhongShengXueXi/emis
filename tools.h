#ifndef TOOLS_H
#define TOOLS_H

#define ID_PATH "data/id.dat"
#define MA_PATH "data/managers.dat"
#define SE_PATH "data/services.dat"

namespace MH{

class Tools
{
	
public:
	void init_id(void);
	int get_id(char m);
	int generator_MarId(void);
	int generator_DeptId(void);
	int generator_EmpId(void);
};

}

#endif //TOOLS_H