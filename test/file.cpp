#include <iostream>
#include <fstream>

#include <string.h>
#include <stdlib.h>

using namespace std;

class Point
{
	char name[20];
	int id;
public:
	Point(const char* _name="",int _id=0)
	{
		strcpy(name,_name);
		id = _id;
	}
	void show(void)
	{
		cout << name << endl;
		cout << id << endl;
	}
	char* getName(void)
	{
		return name;
	}
	int getId(void)
	{
		return id;
	}
};


int main()
{
	ofstream output("data/services.dat",ios::out|ios::trunc);
	if(!output)
	{
		cerr << "out" << endl;
		exit(1);
	}
	ifstream input("data/services.dat",ios::in);
	if(!input)
	{
		cerr << "in" << endl;
		exit(1);
	}
	

	for(int i=1500;i<1520;i++)
	{
		Point p("shi",i);
		output << p.getName() << " " << p.getId() << endl;
	}

	while(1)
	{
		int id;
		char name[20];
		input >> name >> id;
		if(input.eof()) break;
		cout << "=" << name << "=" << id << "=" << endl;
	}
	cout << "----end----" << endl;
}
