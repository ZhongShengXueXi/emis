#include <iostream>
#include <vector>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>

using namespace std;

class Student
{
	int id;
	char name[20];
public:
	Student(int _id=0,const char* _name="")
	{
		id = _id;
		strcpy(name,_name);
	}
	int getId(void)
	{
		return id;
	}
	string getName(void)
	{
		return name;
	}
};

int main()
{
	vector<Student> vs;

	int fd = open("data/managers.dat",O_CREAT|O_RDWR|O_TRUNC,0644);
	if(0 > fd)
	{
		perror("open");
		return -1;
	}
	Student stu1(1000,"zhi");
	write(fd,&stu1,sizeof(Student));
	Student stu2(1001,"shi");
	write(fd,&stu2,sizeof(Student));
	Student stu3(1002,"lin");
	write(fd,&stu3,sizeof(Student));
	Student stu4(1003,"xiao");
	write(fd,&stu4,sizeof(Student));

	Student st(34,"shishishi");
	lseek(fd,SEEK_SET,0);
	read(fd,&st,sizeof(Student));
	vs.push_back(st);
	read(fd,&st,sizeof(Student));
	vs.push_back(st);
	read(fd,&st,sizeof(Student));
	vs.push_back(st);
	read(fd,&st,sizeof(Student));
	vs.push_back(st);

	cout << vs[1].getName() << endl;
	cout << vs.back().getName() << endl;
	vs.pop_back();
	cout << vs.back().getName() << endl;
}
