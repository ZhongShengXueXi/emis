#include <iostream>
#include <vector>
#include "manager.h"
#include "managerdao_file_impl.h"
#include "managerservice.h"
#include "managerservice_impl.h"

#include <string.h>

using namespace std;
using namespace MH;

extern vector<Manager> v_manager;

ManagerService::~ManagerService(void){}//

ManagerServiceImpl::ManagerServiceImpl(void)
{
	dao = new ManagerDaoFileImpl; 
	dao->load();
}

ManagerServiceImpl::~ManagerServiceImpl(void)
{
	dao->save();
	delete dao;
}

void ManagerServiceImpl::addManager(Manager& m)
{
	v_manager.push_back(m);
	cout << "----添加成功----" << endl;
}

void ManagerServiceImpl::deleteManager(int id)
{
	vector<Manager>::iterator startIterator;
	startIterator = v_manager.begin();

	for(int i=0;i<(int)v_manager.size();i++)
	{
		if(id == v_manager[i].getId()) 
		{
			v_manager.erase(startIterator+i);
			cout << "----删除成功----" << endl;
			break;
		}
	}
}

void ManagerServiceImpl::listManager(void)
{
	cout << v_manager.size() << endl;
	for(int i=0;i<(int)v_manager.size();i++)
	{
		cout << v_manager[i].getId() << " " << v_manager[i].getName() << endl;
	}
}

bool ManagerServiceImpl::root_check(void)
{
	char password[20] = {};
	cout << "输入root密码 >";
	while(1)
	{
		cin >> password;
		if(0 == strcmp(password,"5501790")) return true;
		cout << "密码错误,重新输入 >";
	}
}

bool ManagerServiceImpl::mana_check(void)
{
	int id;
	char password[20] = {};
	cout << "输入管理员id >";
	while(1)
	{
		cin >> id;
		for(int i=0;i<(int)v_manager.size();i++)
		{
			if(id == v_manager[i].getId()) 
			{
				cout << "输入管理员密码 >";
				while(1)
				{
					cin >> password;
					if(0 == strcmp(password,v_manager[i].getPassword())) return true;
					cout << "密码错误,重新输入 >";
				}
			}
		}
		cout << "此id不存在,重新输入 >";
	}
}