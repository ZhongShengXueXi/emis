#include <iostream>
#include "employee.h"
#include "department.h"
#include "tools.h"

#include <string.h>

using namespace std;
using namespace MH;

Department::Department(int _d_id,const char* _d_name)
{
	d_id = _d_id;
	strcpy(d_name,_d_name);
}

Department::~Department(void)
{
	Employee* tmp;
	while(1)
	{
		if(v_employeep.empty()) break;
		tmp = v_employeep.back();
		v_employeep.pop_back();
		delete tmp;
	}
}

Department::Department(const Department& de)
{
	d_id = de.d_id;
	strcpy(d_name,de.d_name);
	v_employeep = de.v_employeep;
}

void Department::operator = (const Department& de)
{
	d_id = de.d_id;
	strcpy(d_name,de.d_name);
	v_employeep = de.v_employeep;
}

int Department::getId(void)
{
	return d_id;
}

char* Department::getName(void)
{
	return d_name;
}

vector<Employee*>& Department::getV_employeep(void)
{
	return v_employeep;
}

bool Department::addEmp(Employee* em)
{
	v_employeep.push_back(em);
	return true;
}

bool Department::deleteEmp(int id)
{
	vector<Employee*>::iterator startIterator;
	startIterator = v_employeep.begin();

	for(int i=0;i<(int)v_employeep.size();i++)
	{
		if(id == v_employeep[i]->getId())
		{
			v_employeep.erase(startIterator+i);
			return true;
		}
	}
	return false;
}

void Department::listEmp(void)
{
	for(int i=0;i<(int)v_employeep.size();i++)
	{
		cout << v_employeep[i]->getId() << " " << v_employeep[i]->getName() << " " 
		<< v_employeep[i]->getSex() << " " << v_employeep[i]->getAge() << endl;	
	}
}

bool Department::modifyEmp(int id)
{
	int cmd;
	for(int i=0;i<(int)v_employeep.size();i++)
	{
		if(id == v_employeep[i]->getId())
		{
			cout << "---- 1.修改姓名 ----" << endl;
			cout << "---- 2.修改性别 ----" << endl;
			cout << "---- 3.修改年龄 ----" << endl << endl;
			cout << "---- 0.放弃修改 ----" << endl;

			cin >> cmd;
			switch(cmd)
			{
				case 1 : 
				{
					char name[20] = {};
					cout << "姓名修改为 >";
					cin >> name;
					v_employeep[i]->setName(name);
					return true;
				}
				case 2 :
				{
					bool sex = true;
					cout << "性别修改为 >";
					cin >> sex;
					v_employeep[i]->setSex(sex);
					return true;
				}
				case 3 : 
				{
					int age;
					cout << "年龄修改为 >";
					cin >> age;
					v_employeep[i]->setAge(age);
					return true;
				}
				case 0 : return false;
				default: continue;
			}
		}
	}
	return false;
}
