#include <iostream>

#include "serviceview_console_impl.h"
#include "managerview_console_impl.h"

using namespace std;
using namespace MH;

int main()
{
	ServiceViewConsoleImpl se;
	ManagerViewConsoleImpl ma;
	while(1)
	{
		system("clear");
		cout << "==欢迎使用企业管理信息系统==" << endl << endl;
		cout << "	1.管理系统" << endl;
		cout << "	2.业务系统" << endl << endl;
		cout << "	0.退出系统" << endl;
		cout << ">";
		int cmd;
		cin >> cmd;
		switch(cmd)
		{
			case 1 : ma.root_check();ma.menu();break;
			case 2 : ma.mana_check();se.menu();break;
			case 0 : return 0;
			default: break;
		}
	}
}
