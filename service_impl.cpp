#include <iostream>
#include <vector>
#include "service.h"
#include "department.h"
#include "service_impl.h"

#include "servicedao_file_impl.h"
#include "employee.h"

using namespace std;
using namespace MH;

Service::~Service(void){}

ServiceImpl::ServiceImpl(void)
{
	dao = new ServiceDaoFileImpl;
	dao->load(v_departmentp);
}

ServiceImpl::~ServiceImpl(void)
{
	dao->save(v_departmentp);
	delete dao;
}

bool ServiceImpl::addDept(Department* de)
{
	v_departmentp.push_back(de);
	cout << "----添加成功----" << endl;
	return true;
}

bool ServiceImpl::deleteDept(int id)
{
	for(int i=0;i<(int)v_departmentp.size();i++)
	{
		if(id == v_departmentp[i]->getId())
		{
			Department* de = v_departmentp[i];

			vector<Department*>::iterator startIterator;
			startIterator = v_departmentp.begin();
			v_departmentp.erase(startIterator+i);

			delete de;

			cout << "----删除成功----" << endl;
			return true;
		}
	}
	cout << "----删除失败----" << endl;
	return false;
}

void ServiceImpl::listDept(void)
{
	cout << "部门id    部门名称" << endl;
	for(int i=0;i<(int)v_departmentp.size();i++)
	{
		cout << v_departmentp[i]->getId() << " " << v_departmentp[i]->getName() << endl;
	}
}

bool ServiceImpl::addEmp(Employee* em)
{
	int id;
	cout << "输入员工所属部门id >";
	cout << ">";
	cin >> id;

	for(int i=0;i<(int)v_departmentp.size();i++)
	{
		if(id == v_departmentp[i]->getId())
		{
			v_departmentp[i]->addEmp(em);
			cout << "----添加成功----" << endl;
			return true;
		}
		
	}
	cout << "----添加失败----" << endl;
	return false;
}

bool ServiceImpl::deleteEmp(int id)
{
	int d_id;
	cout << "please input department's id:";
	cout << ">";
	cin >> d_id;

	for(int i=0;i<(int)v_departmentp.size();i++)
	{
		if(id == v_departmentp[i]->getId())
		{
			v_departmentp[i]->deleteEmp(id);
			cout << "----删除成功----" << endl;
			return true;
		}
		
	}
	cout << "----删除失败----" << endl;
	return false;
}

bool ServiceImpl::modifyEmp(int id)
{
	int d_id;
	cout << "输入员工所属部门id >";
	cout << ">";
	cin >> d_id;

	for(int i=0;i<(int)v_departmentp.size();i++)
	{
		if(d_id == v_departmentp[i]->getId())
		{
			v_departmentp[i]->modifyEmp(id);
			cout << "----修改成功----" << endl;
			return true;
		}
	}
	cout << "----修改失败----" << endl;
	return false;
}

void ServiceImpl::listEmp(void)
{
	int d_id;
	cout << "输入员工所属部门id >";
	cout << ">";
	cin >> d_id;

	for(int i=0;i<(int)v_departmentp.size();i++)
	{
		if(d_id == v_departmentp[i]->getId())
		{
			v_departmentp[i]->listEmp();
			return;
		}
	}
}

void ServiceImpl::listAllEmp(void)
{
	for(int i=0;i<(int)v_departmentp.size();i++)
	{
		v_departmentp[i]->listEmp();
	}
}
