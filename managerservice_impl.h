#ifndef MANAGERSERVICE_IMPL_H
#define MANAGERSERVICE_IMPL_H

#include "manager.h"
#include "managerdao.h"
#include "managerservice.h"

namespace MH{

class ManagerServiceImpl : public ManagerService
{
	ManagerDao* dao;
public:
	ManagerServiceImpl(void);
	~ManagerServiceImpl(void);
	void addManager(Manager& m);
	void deleteManager(int id);
	void listManager(void);
	bool root_check(void);
	bool mana_check(void);
};

}

#endif //MANAGERSERVICE_IMPL_H